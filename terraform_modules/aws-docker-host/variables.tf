variable "name" {
  type        = string
  default     = "docker-host"
  description = "Name for deployment"
}

variable "aws_region" {
  type    = string
  default = "us-west-2"
}

variable "app_name" {
  type    = string
  default = "juiceshop"
}

variable "instance_type" {
  default = "t3.large"
}

variable "subnet_id" {
  type        = string
  default     = null
  description = "subnet in which to deploy host"
}

variable "vpc_id" {
  type        = string
  default     = null
  description = "vpc in which to deploy host"
}

variable "key_name" {
  type        = string
  default     = null
  description = "existing key_name in your aws acccount"
}

variable "username" {
  type        = string
  default     = null
  description = "username of owner"
}