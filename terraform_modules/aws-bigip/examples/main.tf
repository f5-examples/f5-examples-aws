module bigip_onboard {
  source = "../../terraform_modules/bigip-onboard"
  bigip_address                       = module.bigip.mgmt_public_ips[0]
  bigip_username                      = "admin"
  bigip_password                      = var.BIGIP_PWD
  bigip_hostname                      = "bigip-a.example.com"
  private_ip                          = module.bigip.private_addresses[0]
  public_ip                           = module.bigip.public_addresses[0]
  advisory_text                       = "BIGIP-A"
}