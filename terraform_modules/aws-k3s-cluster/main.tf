provider "aws" {
  region     = var.aws_region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}

locals {
  name                        = var.name
  install_k3s_version         = var.install_k3s_version
  k3s_cluster_secret          = var.k3s_cluster_secret != null ? var.k3s_cluster_secret : random_password.k3s_cluster_secret.result
  server_instance_type        = var.server_instance_type
  agent_instance_type         = var.agent_instance_type
  agent_image_id              = var.agent_image_id != null ? var.agent_image_id : data.aws_ami.ubuntu.id
  server_image_id             = var.server_image_id != null ? var.server_image_id : data.aws_ami.ubuntu.id
  aws_azs                     = var.aws_azs
  public_subnets              = length(var.public_subnets) > 0 ? var.public_subnets : data.aws_subnet_ids.available.ids
  private_subnets             = length(var.private_subnets) > 0 ? var.private_subnets : data.aws_subnet_ids.available.ids
  server_node_count           = var.server_node_count
  agent_node_count            = var.agent_node_count
  ssh_keys                    = var.ssh_keys
  k3s_deploy_traefik          = var.k3s_deploy_traefik ? "" : "--no-deploy traefik"
  server_k3s_exec             = ""
  agent_k3s_exec              = ""
  domain                      = var.domain
  private_subnets_cidr_blocks = var.private_subnets_cidr_blocks
  public_subnets_cidr_blocks  = var.public_subnets_cidr_blocks
  install_nginx_ingress       = var.install_nginx_ingress
  create_external_nlb         = var.create_external_nlb ? 1 : 0
}

resource "random_password" "k3s_cluster_secret" {
  length  = 30
  special = false
}

resource "aws_instance" "sca-tgw-k3s-dev" {
  ami                         = local.server_image_id
  instance_type               = local.server_instance_type
  subnet_id                   = local.public_subnets[0]
  key_name                    = var.key_name
  associate_public_ip_address = true
  user_data                   = data.template_cloudinit_config.k3s_server.rendered

  tags = {
    Name = "sca-tgw-k3s-dev"
    env         = "dev"
  }
}