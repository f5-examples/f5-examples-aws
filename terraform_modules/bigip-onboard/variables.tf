   
variable "bigip_address" {
  description = "bigip_address"
  default     = "1.1.1.1"
}

variable "bigip_username" {
  description = "username to access bigip"
  type        = string
  default     = "admin"
}

variable "bigip_password" {
  description = "bigip_password for the username provided before"
  type        = string
  default     = "hogusbogusdontuse"
}

variable "config_name" {
  description = "config_name"
  type        = string
  default     = "example.bigip.com"
}

variable "do_json" {
  description = "do_json"
  type        = string
}
