provider "bigip" {
    address   = var.bigip_address
    username  = var.bigip_username
    password  = var.bigip_password
}

resource "null_resource" "bigip_ready" {
  provisioner "local-exec" {
    # Bootstrap script called with private_ip of each node in the clutser
    command = "while ! curl -fsku $CREDS https://$bigip_address/mgmt/shared/declarative-onboarding/info; do echo trying again&sleep 1; done"
    environment = {
      CREDS                = "${var.bigip_username}:${var.bigip_password}"
      bigip_address        = var.bigip_address
    }
    
  }

}

resource "bigip_do"  "do-example" {
  depends_on                       = [null_resource.bigip_ready]
  do_json = var.do_json
  config_name  = var.config_name
}