output "vpc-1-id" {
  description = "vpc1 id"
  value       = aws_vpc.vpc-1.id
}

output "vpc-1-public-subnets-ids" {
  description = "vpc-1-public-subnets-ids"
  value       = [aws_subnet.vpc-1-sub-pub-a.id , aws_subnet.vpc-1-sub-pub-b.id]
}

output "vpc-1-mgmt-subnets-ids" {
  description = "vpc-1-mgmt-subnets-ids"
  value       = [aws_subnet.vpc-1-sub-mgmt-a.id , aws_subnet.vpc-1-sub-mgmt-b.id]
}

output "vpc-1-private-subnets-ids" {
  description = "vpc-1-private-subnets-ids"
  value       = [aws_subnet.vpc-1-sub-priv-a.id , aws_subnet.vpc-1-sub-priv-b.id]
}

output "vpc-1-sec-group-vpc-1-web" {
  description = "sec-group-vpc-1-web"
  value       = aws_security_group.sec-group-vpc-1-web.id
}

output "sec-group-vpc-1-ssh-icmp" {
  description = "sec-group-vpc-1-ssh-icmp"
  value       = aws_security_group.sec-group-vpc-1-ssh-icmp.id
}


output "vpc-10-id" {
  description = "vpc10 id"
  value       = aws_vpc.vpc-10.id
}

output "vpc-20-id" {
  description = "vpc20 id"
  value       = aws_vpc.vpc-20.id
}

output "vpc-30-id" {
  description = "vpc30 id"
  value       = aws_vpc.vpc-30.id
}

output "subnet_ids" {
  description = "subnet_ids"
  value       = [
      aws_subnet.vpc-1-sub-pub-a.id,
      aws_subnet.vpc-1-sub-pub-b.id,
      aws_subnet.vpc-1-sub-priv-a.id,
      aws_subnet.vpc-1-sub-priv-b.id,
      aws_subnet.vpc-1-sub-mgmt-a.id,
      aws_subnet.vpc-1-sub-mgmt-b.id,
      aws_subnet.vpc-10-sub-a.id,
      aws_subnet.vpc-10-sub-b.id,
      aws_subnet.vpc-20-sub-a.id,
      aws_subnet.vpc-20-sub-b.id,
      aws_subnet.vpc-30-sub-a.id,
      aws_subnet.vpc-30-sub-b.id
      ]
}

output "vpc-10-subnets-ids" {
  description = "vpc-10-subnets-ids"
  value       = [aws_subnet.vpc-10-sub-a.id , aws_subnet.vpc-10-sub-b.id]
}

output "jumphost_public_ip" {
  value = aws_instance.sca-tgw-instance1-shared.public_ip
}

output "key_name" {
  value = aws_key_pair.sca-tgw-keypair.key_name 
}
