
variable "name" {
  type        = string
  default     = "sca-tgw"
  description = "Name for deployment"
}

variable "aws_region" {
  default = "us-west-2"
}

variable "az1" {
  default = "us-west-2a"
}

variable "az2" {
  default = "us-west-2b"
}

variable "username" {
}

variable "public_key" {
  description = "ssh public key in a text format"
}

variable "instance_type" {
  default = "t3.large"
}