{
    "schemaVersion": "1.0.0",
    "class": "Device",
    "async": true,
    "webhook": "https://example.com/myHook",
    "label": "my BIG-IP declaration for declarative onboarding",
    "Common": {
        "class": "Tenant",
        "hostname":  "${hostname}",
        "myNtp": {
            "class": "NTP",
            "servers": [
                "0.pool.ntp.org",
                "1.pool.ntp.org",
                "2.pool.ntp.org"
            ],
            "timezone": "UTC"
        },
        "internal": {
            "class": "VLAN",
            "tag": 4093,
            "mtu": 1500,
            "interfaces": [
                {
                    "name": "1.2",
                    "tagged": false
                }
            ],
            "cmpHash": "src-ip"
        },
        "internal-self": {
            "class": "SelfIp",
            "address":  "${private_ip}",
            "vlan": "internal",
            "allowService": "default",
            "trafficGroup": "traffic-group-local-only"
        },
        "external": {
            "class": "VLAN",
            "tag": 4094,
            "mtu": 1500,
            "interfaces": [
                {
                    "name": "1.1",
                    "tagged": false
                }
            ],
            "cmpHash": "src-ip"
        },
        "external-self": {
            "class": "SelfIp",
            "address": "${public_ip}",
            "vlan": "external",
            "allowService": "default",
            "trafficGroup": "traffic-group-local-only"
        },
        "internal-route": {
            "class": "Route",
            "gw": "${internal_gw}",
            "network": "10.0.0.0/8",
            "mtu": 1500
        },
        "dbvars": {
        	"class": "DbVariables",
        	"ui.advisory.enabled": true,
        	"ui.advisory.color": "green",
        	"ui.advisory.text": "${advisory_text}"
        }
    }
}