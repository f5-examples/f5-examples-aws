provider "bigip" {
    address   = var.bigip_address
    username  = var.bigip_username
    password  = var.bigip_password
}

resource "bigip_as3" "as3_service" {
  as3_json = var.as3_json
  config_name  = var.config_name
}