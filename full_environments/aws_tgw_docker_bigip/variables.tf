
variable "USERNAME" {
}

variable "BIGIP_PWD" {
}

variable "PUBLIC_KEY" {
}

variable "AWS_ACCESS_KEY_ID" {
}

variable "AWS_SECRET_ACCESS_KEY" {
}

variable "az1" {
  default = "us-west-2a"
}

variable "az2" {
  default = "us-west-2b"
}

variable "aws_region" {
  default = "us-west-2"
}

variable "deployment_name" {
  default = "sca-tgw"
}
