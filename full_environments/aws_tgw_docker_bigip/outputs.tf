output "jumphost_public_ip" {
  description = "public ip of jumphost"
  value       = module.f5-rs-aws-tgw-network.jumphost_public_ip
}

output "dev_docker_a_public_ip" {
  description = "public ip of dev_docker_a"
  value       = module.dev-docker-a.docker_host_public_ip
}

output "dev_docker_b_public_ip" {
  description = "public ip of dev_docker_b"
  value       = module.dev-docker-b.docker_host_public_ip
}

output "mgmt_public_ips" {
  description = "mgmt_public_ips"
  value       = module.bigip.mgmt_public_ips
}
